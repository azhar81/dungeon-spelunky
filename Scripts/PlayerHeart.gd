extends HBoxContainer

var heart_full = preload("res://assets/heart/full.png")
var heart_empty = preload("res://assets/heart/empty.png")
var heart_half = preload("res://assets/heart/half.png")


func update_health(value):
	for i in get_child_count():
		if value > i * 2 + 1:
			get_child(i).texture = heart_full
		elif value > i * 2:
			get_child(i).texture = heart_half
		else:
			get_child(i).texture = heart_empty
