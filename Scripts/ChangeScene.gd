extends Area2D

export (String) var scene_name = ""

func _on_ChangeScene_body_entered(body):
	if body.get_name() == "Player":
		Global.previous_score = Global.current_score
		Global.previous_health = Global.player_health
		get_tree().change_scene("res://Scenes/" + scene_name + ".tscn")
