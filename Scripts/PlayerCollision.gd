extends Area2D

export (int) var damage = 2

func _process(_delta):
	for body in get_overlapping_bodies():
		body.hurt(damage)
