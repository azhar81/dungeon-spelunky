extends CanvasLayer

func _on_Return_pressed():
	get_tree().paused = false
	for i in get_children():
		i.hide()

func _on_Main_menu_pressed():
	get_tree().paused = false
	Global.save_game()
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
