extends MarginContainer

func _on_LinkButton_pressed():
	get_tree().change_scene("res://Scenes/Level 1.tscn")
	Global.player_health = 10
	Global.current_score = 0
	Global.save_game()

func _on_LinkButton2_pressed():
	Global.load_game()

func _on_LinkButton3_pressed():
	get_tree().quit()
