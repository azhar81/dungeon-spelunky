extends KinematicBody2D

export (int) var move_speed = 100
export (int) var knockback = 200
export (int) var dash_speed = 300

var velocity = Vector2()
var is_hurt = false
var can_throw = true
var controllable = true
var dagger = preload("res://Scenes/Weapon.tscn")
var is_dashing = false
var can_dash = true

onready var _sprite = $AnimatedSprite
onready var foot = $footstep
onready var heartHUD = $CanvasLayer/HBoxContainer
onready var highScoreHUD = $CanvasLayer/VBoxContainer/HighScore
onready var scoreHUD = $CanvasLayer/VBoxContainer/Score
onready var invinTimer = $InvincibilityTimer
onready var throwTimer = $ThrowTimer
onready var dashDuration = $DashDuration
onready var dashTimer = $DashTimer
onready var menu = $Menu

var saved = preload("res://Scenes/saved.tscn")

func _ready():
	for i in menu.get_children():
		i.hide()
	updateHealthHUD()
	Global.current_score = Global.previous_score
	Global.player_health = Global.previous_health
	updateHighScoreHUD()
	updateScoreHUD()
	Global.current_scene = get_tree().get_current_scene().get_name()
	Global.save_game()

func get_input():
	var mouse_pos = get_global_mouse_position()
	var player_pos = get_global_position()
	
	if not foot.is_playing():
		foot.play()
	
	velocity = Vector2.ZERO
	
	var speed = dash_speed if is_dashing else move_speed
	
	if Input.is_action_pressed("ui_right"):
		velocity.x += speed
		
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
	
	if Input.is_action_pressed('ui_up'):
		velocity.y -= speed
		
	if Input.is_action_pressed('ui_down'):
		velocity.y += speed
		
	if Input.is_action_just_pressed("dash") and can_dash:
		is_dashing = true
		can_dash = false
		collision_layer = 0
		collision_mask = 1
		dashDuration.start()
	
	if Input.is_action_just_pressed("throw"):
		if can_throw:
			throw(mouse_pos, player_pos)
	
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().paused = true
		for i in menu.get_children():
			i.show()

	# animation follow velocity vector
	if not is_hurt:
		if velocity == Vector2.ZERO:
			_sprite.play("Idle")
		else:
			_sprite.play("Move")
	else:
		_sprite.play("Hurt")
	
	if velocity == Vector2.ZERO:
		foot.stop()
		
	# facing direction follow position of mouse in relation to player position
	if mouse_pos.x < player_pos.x:
		_sprite.set_flip_h(true)
	else:
		_sprite.set_flip_h(false)

func _process(_delta):
	if controllable:
		get_input()
	velocity = move_and_slide(velocity)

func hurt(damage):
	if !is_hurt && !is_dashing:
		invinTimer.start()
		is_hurt = true
		Global.player_health -= damage
		if Global.player_health <= 0:
			dead()
		updateHealthHUD()
		
func updateHealthHUD():
	heartHUD.update_health(Global.player_health)

func heal(n):
	Global.player_health += n
	if Global.player_health > 10:
		Global.player_health = 10
	updateHealthHUD()
	
func dead():
	controllable = false
	velocity = Vector2.ZERO
	if Global.current_score > Global.high_score:
		Global.high_score = Global.current_score
	Global.current_score = 0
	Global.player_health = 10
	Global.previous_health = 10
	Global.previous_score = 0
	Global.current_scene = "Level 1"
	Global.save_game()
	if get_tree().change_scene("res://Scenes/GameOver.tscn") != OK:
		print("An unexpected error occured")

func add_score(n):
	Global.current_score += n
	updateScoreHUD()

func updateHighScoreHUD():
	highScoreHUD.text = "High Score : " + str(Global.high_score)
	
func updateScoreHUD():
	scoreHUD.text = "Score : " + str(Global.current_score)
	
func _on_InvincibilityTimer_timeout():
	is_hurt = false

func throw(direction, location):
	var throwable = dagger.instance()
	get_parent().add_child(throwable)
	
	throwable.position = location
	throwable.look_at(direction)
	throwable.velocity = throwable.position.direction_to(direction)
	
	can_throw = false
	throwTimer.start()

func _on_ThrowTimer_timeout():
	can_throw = true

func _on_DashDuration_timeout():
	is_dashing = false
	collision_mask = 5
	collision_layer = 2
	dashTimer.start()

func _on_DashTimer_timeout():
	can_dash = true
