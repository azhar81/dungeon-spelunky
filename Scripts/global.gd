extends Node

var player_health = 10
var previous_health = 10
var current_scene = "Test"
var high_score = 0
var previous_score = 0
var current_score = 0
	
func _ready():
	load_global_variables()
	
func save_game():
	var save_game = File.new()
	save_game.open("user://savegame.save", File.WRITE)
	save_game.store_line(to_json(save()))
	save_game.close()
	
func load_global_variables():
	var load_game = File.new()
	if not load_game.file_exists("user://savegame.save"):
		return # Error! We don't have a save to load.
	
	load_game.open("user://savegame.save", File.READ)
	
	var node_data = parse_json(load_game.get_line())
	high_score = node_data["high_score"]
	
	load_game.close()
	
func load_game():
	var load_game = File.new()
	if not load_game.file_exists("user://savegame.save"):
		return # Error! We don't have a save to load.
	
	load_game.open("user://savegame.save", File.READ)
	
	var node_data = parse_json(load_game.get_line())
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://Scenes/" + node_data["current_scene"] + ".tscn")
	previous_health = node_data["previous_health"]
	previous_score = node_data["previous_score"]
	
	load_game.close()
	
func save():
	var save_dict = {
		"previous_health" : previous_health,
		"current_scene" : current_scene,
		"high_score" : high_score,
		"previous_score" : previous_score
	}
	return save_dict
