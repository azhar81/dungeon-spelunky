extends KinematicBody2D

export (int) var speed = 60
export (int) var health = 5
export (int) var points = 100

onready var animation = $AnimatedSprite
onready var invinTimer = $invincibilityTimer
onready var dashTimer = $DashTimer
onready var dashDuration = $DashDuration

var player = null
var player_node = null
var velocity = Vector2()
var is_hurt = false
var dash_ready = true
var is_dashing = false
var dash_direction = null
var is_dead = false

func _process(_delta):
	velocity = Vector2.ZERO
	
	if is_dead:
		animation.play("Death")
		
	elif not is_hurt:
		movement()
	else:
		animation.play("Hurt")
	
	velocity = move_and_slide(velocity)

func movement():
	if player:
		var player_pos = player.position
		
		if dash_ready:
			dash_direction = player_pos
			dash()
		
		if is_dashing:
			velocity = position.direction_to(dash_direction) * speed
			
		if position.x > player_pos.x:
			animation.set_flip_h(true)
		else:
			animation.set_flip_h(false)
		
		animation.play("Run")
	else:
		animation.play("Idle")

func _on_PlayerDetect_body_entered(body):
	player = body
	player_node = body

func _on_PlayerDetect_body_exited(_body):
	player = null

func hurt(n):
	if not is_hurt:
		invinTimer.start()
		is_hurt = true
		health -= n
		if health <= 0:
			die()

func die():
	player_node.add_score(points)
	collision_mask = 0
	collision_layer = 0
	$PlayerCollision.queue_free()
	is_dead = true

func dash():
	dashTimer.start()
	dashDuration.start()
	dash_ready = false
	is_dashing = true

func _on_invincibilityTimer_timeout():
	is_hurt = false

func _on_DashTimer_timeout():
	dash_ready = true


func _on_DashDuration_timeout():
	is_dashing = false

func _on_AnimatedSprite_animation_finished():
	if animation.animation == "Death":
		queue_free()
