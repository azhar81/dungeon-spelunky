# Concept:

This game is a top-down shooter where you shoot (Throw knives) at enemies and gain points for killing them. In this game, you'll need to progress through the stage while trying to get your enemies health to zero while keeping yours above zero.

You'll play as Dink, a green-robed blonde guy that likes exploring dungeons and killing unorthodox creatures. Dink's weapon of choice is an unlimited amount of daggers which he's been throwing since he was a child.

# Diversifier:

- Kepakan Sayap Baliho - Implementasi fitur yang menutupi sebagian layar player
- Big Iron - Implementasikan ranged combat/duel combat/senjata api
- Ave Maria - Player tidak terpengaruh environment selama suatu periode

# Controls:

- WASD keys / Arrow keys : Control the character
- Mouse : Control where you're shooting
- Left Mouse Button : Throw the dagger
- Spacebar : Dash through hurty things

# Assets:

- Sprites \
https://rustybulletgames.itch.io/colored-explosions-asset-pack \
https://0x72.itch.io/dungeontileset-ii
 
- Fonts \
https://www.fontspace.com/edge-of-the-galaxy-font-f45748 \
https://vrtxrry.itch.io/dungeonfont
 
- Audio \
https://kenney.nl/assets/rpg-audio \
https://wingless-seraph.net/en/material-music_dangeon.html \
https://freesound.org/people/ProjectsU012/sounds/341695/

## itch.io page
https://azhar82.itch.io/dungeon-spelunky
